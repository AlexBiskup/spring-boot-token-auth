package at.mindcloud.tokenauth.configuration

enum class TokenPrefix(
    val value: String
) {
    BEARER("Bearer "), NO("")
}