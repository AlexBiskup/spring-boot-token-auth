package at.mindcloud.tokenauth.filter

import at.mindcloud.tokenauth.jwt.service.TokenService
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class TokenAuthenticationFilter(
    private val tokenService: TokenService,
    private val userDetailsService: UserDetailsService

): OncePerRequestFilter() {

    @Value("\${token-auth.headerName}")
    private var tokenHeaderName: String = "Authorization"

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val token: String? = request.getHeader(tokenHeaderName)
        if (noAuthenticationIsPresent() && token != null) {
            tokenService.validatePrefixedToken(token, request.remoteAddr)
            val userDetails = getUserDetailsByToken(token)
            val authentication = buildAuthenticationFromUserDetails(userDetails, request)
            setAuthenticationToSecurityContext(authentication)
        }
        filterChain.doFilter(request, response)
    }

    private fun noAuthenticationIsPresent(): Boolean {
        return SecurityContextHolder.getContext().authentication == null
    }

    private fun getUserDetailsByToken(token: String): UserDetails {
        val username = tokenService.getUsernameFromPrefixedToken(token)
        return userDetailsService.loadUserByUsername(username)
    }

    private fun buildAuthenticationFromUserDetails(userDetails: UserDetails, request: HttpServletRequest): Authentication {
        val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
        authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
        return authentication
    }

    private fun setAuthenticationToSecurityContext(authentication: Authentication) {
        SecurityContextHolder.getContext().authentication = authentication
    }
}
