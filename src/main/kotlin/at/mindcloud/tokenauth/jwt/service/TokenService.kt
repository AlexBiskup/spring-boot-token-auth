package at.mindcloud.tokenauth.jwt.service

import org.springframework.security.core.userdetails.UserDetails
import java.util.*

interface TokenService {

    fun generateToken(userDetails: UserDetails, remoteAddress: String): String

    fun validatePrefixedToken(token: String, remoteAddress: String)

    fun getUsernameFromPrefixedToken(token: String): String

    fun getExpirationDateFromPrefixedToken(token: String): Date

    fun refreshPrefixedToken(token: String, refreshToken: String): String

    fun getRefreshTokenFromPrefixedToken(token: String): String

}