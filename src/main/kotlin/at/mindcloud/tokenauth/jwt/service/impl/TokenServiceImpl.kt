package at.mindcloud.tokenauth.jwt.service.impl

import at.mindcloud.tokenauth.configuration.TokenPrefix
import at.mindcloud.tokenauth.configuration.TokenPrefix.BEARER
import at.mindcloud.tokenauth.jwt.service.TokenService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm.HS512
import io.jsonwebtoken.security.Keys.secretKeyFor
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*

@Service
class TokenServiceImpl: TokenService {

    private val key = secretKeyFor(HS512)

    @Value("\${token-auth.expirationTimeInMinutes}")
    private var expirationTimeInMinutes: Long = 15

    @Value("\${token-auth.tokenPrefix}")
    private var tokenPrefix: TokenPrefix = BEARER

    override fun generateToken(userDetails: UserDetails, remoteAddress: String): String {
        val token = createToken(userDetails.username, remoteAddress)
        return addPrefixToToken(token)
    }

    override fun validatePrefixedToken(token: String, remoteAddress: String) {
        val claims = getClaimsFromToken(removePrefixFromToken(token))
        if (remoteAddress != claims[CLAIM_KEY_REMOTE_ADDRESS] || isTokenExpired(claims.expiration)) {
            throw InvalidTokenException()
        }
    }

    override fun refreshPrefixedToken(token: String, refreshToken: String): String {
        val claims = getClaimsFromToken(removePrefixFromToken(token))
        if (refreshToken == claims[CLAIM_KEY_REFRESH_TOKEN] as String) {
            val username = claims.subject
            val remoteAddress = claims[CLAIM_KEY_REMOTE_ADDRESS] as String
            return createToken(username, remoteAddress)
        }
        throw InvalidRefreshTokenException()
    }

    private fun createToken(username: String, remoteAddress: String): String {
        val claims = HashMap<String, Any>()
        claims[CLAIM_KEY_REMOTE_ADDRESS] = remoteAddress
        claims[CLAIM_KEY_REFRESH_TOKEN] = UUID.randomUUID().toString()
        val created = Date(System.currentTimeMillis())
        val expiration = calculateExpirationDate(created)

        return Jwts.builder()
            .setClaims(claims)
            .setSubject(username)
            .setIssuedAt(created)
            .setExpiration(expiration)
            .signWith(key)
            .compact()
    }

    override fun getRefreshTokenFromPrefixedToken(token: String): String {
        return getClaimsFromToken(removePrefixFromToken(token))[CLAIM_KEY_REFRESH_TOKEN] as String
    }


    override fun getUsernameFromPrefixedToken(token: String): String {
        return getClaimsFromToken(removePrefixFromToken(token)).subject
    }


    override fun getExpirationDateFromPrefixedToken(token: String): Date {
        return getClaimsFromToken(removePrefixFromToken(token)).expiration
    }

    private fun isTokenExpired(expiration: Date): Boolean {
        return Date(System.currentTimeMillis()).after(expiration)
    }

    private fun getClaimsFromToken(token: String): Claims {
        return Jwts.parser()
            .setSigningKey(key)
            .parseClaimsJws(token)
            .body
    }

    private fun calculateExpirationDate(created: Date): Date {
        return Date(created.time + minutesToMilliseconds(expirationTimeInMinutes))
    }

    private fun minutesToMilliseconds(minutes: Long): Long {
        return minutes * MILLISECONDS_PER_MINUTE
    }

    private fun addPrefixToToken(token: String): String {
        return "${tokenPrefix.value}$token"
    }

    private fun removePrefixFromToken(token: String): String {
        return token.replace(tokenPrefix.value, "")
    }

}

private const val CLAIM_KEY_REMOTE_ADDRESS = "ra"
private const val CLAIM_KEY_REFRESH_TOKEN = "rt"

private const val MILLISECONDS_PER_MINUTE = 60000

class InvalidRefreshTokenException: RuntimeException()

class InvalidTokenException: RuntimeException()
