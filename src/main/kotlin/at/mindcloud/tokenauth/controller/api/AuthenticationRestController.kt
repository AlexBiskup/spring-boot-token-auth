package at.mindcloud.tokenauth.controller.api

import at.mindcloud.tokenauth.jwt.service.TokenService
import io.swagger.annotations.Api
import org.springframework.http.HttpStatus.UNAUTHORIZED
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/auth")
@Api(description = "resource for authenticating users", tags = ["authentication"])
class AuthenticationRestController(
    private val userDetailsService: UserDetailsService,
    private val tokenService: TokenService,
    private val authenticationManager: AuthenticationManager
) {

    @PostMapping
    fun getAuthToken(@RequestBody authenticationRequest: AuthenticationRequest, request: HttpServletRequest): AuthenticationResponse {
        try {
            authenticate(authenticationRequest)
            val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.username)
            val token = tokenService.generateToken(userDetails, request.remoteAddr)
            return createAuthenticationResponse(token)
        } catch (exception: Exception) {
            throw BadCredentialsException("Bad credentials.")
        }
    }

    @PostMapping("/refresh")
    fun refreshAuthToken(@RequestBody refreshTokenRequest: RefreshTokenRequest): AuthenticationResponse {
        val refreshedToken = tokenService.refreshPrefixedToken(refreshTokenRequest.token, refreshTokenRequest.refreshToken)
        return createAuthenticationResponse(refreshedToken)
    }

    private fun authenticate(authenticationRequest: AuthenticationRequest) {
        val username = authenticationRequest.username
        val password = authenticationRequest.password
        Objects.requireNonNull(username)
        Objects.requireNonNull(password)
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
    }

    private fun createAuthenticationResponse(token: String): AuthenticationResponse {
        val expirationDate = tokenService.getExpirationDateFromPrefixedToken(token)
        val refreshToken = tokenService.getRefreshTokenFromPrefixedToken(token)
        return AuthenticationResponse(token, refreshToken, expirationDate)
    }

}

data class AuthenticationRequest(
    val username: String,
    val password: String
)

data class AuthenticationResponse(
    val token: String,
    val refreshToken: String,
    val expirationDate: Date
)

data class RefreshTokenRequest(
    val token: String,
    val refreshToken: String
)

@ResponseStatus(UNAUTHORIZED)
class BadCredentialsException(
    override val message: String
): RuntimeException(message)
